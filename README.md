# NREL 5 MW DISCON and Interface for HAWC2

This repo contains files necessary to couple the NREL 5 MW discon
controllers, including those used in the OC projects, with HAWC2.
This repo houses the source code for the interface DLL; the source
code for the DISCON controllers is scraped from an
[NREL GitHub repository](https://github.com/OpenFAST/r-test).

You can download the compiled DLLs from the [release page](https://gitlab.windenergy.dtu.dk/OpenLAC/servosandutilities/nrel-5mw-discon/-/releases)
by clicking the tag, then "Download - compile_dlls".

Be sure to use the versions (i.e., 32- or 64-bit) that match your
HAWC2 executable.

## Which DLLs to use

There are 3 sets of 32- and 64-bit DLLs.
To run a specific NREL 5 MW model, you will need to place two DLL
files in the `control/` directory of your NREL 5 MW model: the 
`nrel_5mw_interface` DLL and the DISCON DLL that corresponds to
the model you want to run.

The `nrel_5mw_interface` DLL is glue code that interfaces HAWC2 with
the DISCON controller. In particular, it passes the swap array back
and forth and calls the DISCON DLL. It also defines which control
channels can be accessed as HAWC2 outputs.

There are two sets of DISCON DLLs: `DISCON` and `DISCON_OC3`.
The set corresponding to different NREL 5 MW models is given below:

* NREL 5 MW - DISCON
* OC3
  * Phase I (Monopile) – DISCON
  * Phase 2 (Extended Monopile) – DISCON
  * Phase 3 (Tripod) – DISCON
  * Phase 4 (Floating Spar Buoy) – DISCON_OC3
* OC4
  * Phase 1 (Jacket) – DISCON
  * Phase 2 (Semisub) – DISCON_OC3

**Inportant!** Once copied, your DISCON DLL must be renamed to `DISCON.dll`
in the control folder, because this is the file that the interface DLL will try
to open. If you are using the file already named `DISCON.dll`, you don't need
to rename it.

## Blocks in an htc file

To use the interface, your htc file must have the correct block structure.
Example input and output blocks are given below.
You can also download the full NREL 5 MW HAWC files from the repo:
https://gitlab.windenergy.dtu.dk/hawc-reference-models/nrel-5mw-rwt

### Input block in HAWC2

Here is an example of a `type2_dll` subblock with the interface.
Note that the subblock should be nested under the DLL block.

```
  begin type2_dll; 1. discon controller interface
    name nrel_5mw_interface ;
    filename  ./control/nrel_5mw_interface.dll ;
    dll_subroutine_init initialize_type2dll ;
    dll_subroutine_update update_type2dll ;
    arraysizes_init  100 1 ;
    arraysizes_update  100 100 ;
  ;
    begin init ;  parameters needed to initialize DLL
      constant 1  97    ; Gear ratio [-]
      constant 2   3    ; Number of blades [-]
      constant 3   1    ; Enable DLL log [0=disable, 1=enable]
    end init ;
  ;
    begin output;  HAWC2 channels passed to the interface DLL
      general status                            ;  Status [-]
      general time                              ;  Time [s]
      constraint bearing1 shaft_rot 1 only 2    ;  LSS [rad/s]
      constraint bearing2 pitch1 1 only 1       ;  Blade1 pitch angle [rad]
      constraint bearing2 pitch2 1 only 1       ;  Blade1 pitch angle [rad]
      constraint bearing2 pitch3 1 only 1       ;  Blade1 pitch angle [rad]
      wind free_wind_hor 1 0.0 0.0 -90.0 only 1;  Hub-height wind speed [m/s]
    end output;           
  ;
    begin actions;  actions applied to the turbine   
      mbdy moment_int shaft 1 3 shaft towertop 2 ;  Generator LSS torque [Nm]
      constraint bearing2 angle pitch1 ;            Angle pitch1 bearing    [rad]
      constraint bearing2 angle pitch2 ;            Angle pitch2 bearing    [rad]
      constraint bearing2 angle pitch3 ;            Angle pitch3 bearing    [rad]
    end actions; 
  ;  
  end type2_dll;
```

### Output block in HAWC2

Using the `dll inpvec` output in HAWC2, the following channels are
available from the DLL:

1. Demanded generator torque [Nm]
2. Blade 1 pitch [rad]
3. Blade 2 pitch [rad]
4. Blade 3 pitch [rad]
5. Electrical power [kW]
6. Generator speed [rpm]
