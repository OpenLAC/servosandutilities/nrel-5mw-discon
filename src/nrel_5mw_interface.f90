! =======================================================================
! Interface DLL between HAWC2 and NREL (and OC) DISCON DLLs.
! https://gitlab.windenergy.dtu.dk/HAWC2/HAWC2-DLLS/nrel-5mw-discon
! =======================================================================

MODULE NREL_5MW_INTERFACE

   USE DFWIN
   USE, INTRINSIC :: ISO_C_BINDING
   IMPLICIT NONE

   
   !----------------------------------------------------------------------------------------------!
   ! Global variables and functions
   !----------------------------------------------------------------------------------------------!
   
   ! Constants
   REAL(C_FLOAT) pi, degrad, raddeg, GenEff
   parameter(pi = 3.14159265358979, degrad = 0.01745329251994, raddeg = 57.295779513093144, GenEff = 0.000944)
   
   ! Define Global Variables for logging and debug
   LOGICAL          :: EnableDllLog = .false.   ! Enable writing to log-file when value is .true.
   REAL(C_DOUBLE)   :: SelectedCtrl, EnableLog

   ! Define Global Variables for TYPE2_DLL
   REAL(C_DOUBLE) GearRatio, NumBld

   ! Define Variables and Interfaces for Library processes
   integer(C_INTPTR_T) :: proc_address

   ABSTRACT INTERFACE
      SUBROUTINE BladedDLL_Procedure ( avrSWAP, aviFAIL, accINFILE, avcOUTNAME, avcMSG )  BIND(C)
         USE, INTRINSIC :: ISO_C_Binding
		 IMPLICIT NONE
		 !DEC$ ATTRIBUTES STDCALL :: BladedDLL_Procedure
         REAL(C_FLOAT),          INTENT(INOUT) :: avrSWAP   (*)  !< DATA
         INTEGER(C_INT),         INTENT(INOUT) :: aviFAIL        !< FLAG  (Status set in DLL and returned to simulation code)
         CHARACTER(KIND=C_CHAR), INTENT(IN)    :: accINFILE (*)  !< INFILE
         CHARACTER(KIND=C_CHAR), INTENT(IN)    :: avcOUTNAME     !< OUTNAME (Simulation RootName)
         CHARACTER(KIND=C_CHAR), INTENT(INOUT) :: avcMSG         !< MESSAGE (Message from DLL to simulation code [ErrMsg])         
      END SUBROUTINE BladedDLL_Procedure
   END INTERFACE

   CONTAINS

   
   !----------------------------------------------------------------------------------------------!
   ! Initialize subroutine for TYPE2_DLL
   !----------------------------------------------------------------------------------------------!
   
   SUBROUTINE initialize_type2dll(arrayin, arrayout)
   
      USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE
      !DEC$ ATTRIBUTES DLLEXPORT, C, ALIAS:'initialize_type2dll' :: initialize_type2dll
	  INTEGER(C_INTPTR_T) :: module_handle

      REAL(C_DOUBLE), DIMENSION(3) :: arrayin   ! from init block in htc
      REAL(C_DOUBLE), DIMENSION(1) :: arrayout  ! array passed to HAWC2 from DLL
      INTEGER                      :: FileStat
      REAL(C_DOUBLE)               :: SelectedCtrl, EnableLog
      
      ! Inizialize Output array
      arrayout = 0.0                                 ! Set return array to a value
      
      ! Load values from init block in htc file
      print*, 'Initializing nrel_5mw_interface DLL...'
      GearRatio = arrayin(1)  ! 1: Gearbox Ratio [-]
	  NumBld    = arrayin(2)  ! 2: Number of blades  [-]
      EnableLog = arrayin(3)  ! 3: Enable LACmusCtrlDllLog [1=enable]
      IF (EnableLog == 0) THEN
         EnableDllLog = .false.
      ELSE
         EnableDllLog = .true.
      ENDIF	  
      
      ! Initialize and open DLL Interface
!DEC$ IF DEFINED(_WIN32)
      module_handle = LoadLibrary(C_CHAR_'./control/DISCON.dll' // C_NULL_CHAR)
!DEC$ ELSE IF DEFINED(_WIN64)
      module_handle = LoadLibrary(C_CHAR_'./control/DISCON_64.dll' // C_NULL_CHAR)
!DEC$ ELSE IF DEFINED(__linux__)
      module_handle = LoadLibrary(C_CHAR_'./control/libDISCON.so' // C_NULL_CHAR)
!DEC$ END IF
      IF (module_handle .eq. 0) STOP 'Unable to load DISCON.dll! (type2_dll)'
   
      ! Get pointer to DISCON subroutine
      proc_address = GetProcAddress( module_handle, C_CHAR_'DISCON' // C_NULL_CHAR )
      IF (proc_address .eq. 0) STOP 'Unable to obtain DISCON address in DLL! (type2_dill)'
      
      ! If requested, start new log-file and add some header text
      IF (EnableDllLog .eqv. .true.) THEN      
         OPEN(UNIT=99, IOSTAT=FileStat, FILE='_DllLog.txt')         
         IF (FileStat == 0) CLOSE(99, STATUS='delete')
         OPEN(UNIT=99, FILE='_DllLog.txt', STATUS="new", ACTION="write") 
         WRITE (99,*) "This file contains select values from the DISCON swap array"
         WRITE (99,*) "Initialization array: ", arrayin(1), arrayin(2), arrayin(3)
         WRITE (99,*) "------------------------------------------"
         WRITE (99,*) " 2_Time       20_GenSpd        47_RotTq     45_Pitch        61_NumBld "                          
         CLOSE(UNIT=99)
      ENDIF
   END SUBROUTINE 
   
   
   !----------------------------------------------------------------------------------------------!
   ! Update subroutine for TYPE2_DLL
   !----------------------------------------------------------------------------------------------!
   
   SUBROUTINE update_type2dll(arrayin, arrayout)
   
      !DEC$ ATTRIBUTES DLLEXPORT, C, ALIAS:'update_type2dll' :: update_type2dll
      
	  USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE
      REAL(C_DOUBLE), DIMENSION(18) :: arrayin   ! from output subblock in htc
      REAL(C_DOUBLE), DIMENSION(6)  :: arrayout  ! returned to HAWC2
	  INTEGER(C_INT)                :: callno = 0, stepno = 0
	  REAL(C_FLOAT)                    deltat, CollectivePitch
      
      PROCEDURE(BladedDLL_Procedure), BIND(C), POINTER :: bladed_proc
   
      ! Passed Variables:
      REAL(C_FLOAT) , DIMENSION(164)             :: avrSWAP=0.d0             ! The swap array: used to pass data to and from the DLL controller [see Bladed DLL documentation]
      INTEGER(C_INT)                             :: aviFAIL=0                !< FLAG  (Status set in DLL and returned to simulation code)
      !!! Depends on line 234 !!!
	  CHARACTER(KIND=C_CHAR) , DIMENSION(24)     :: accINFILE                !< INFILE
      CHARACTER(KIND=C_CHAR)                     :: avcOUTNAME=C_NULL_CHAR   !< OUTNAME (Simulation RootName)
      CHARACTER(KIND=C_CHAR)                     :: avcMSG                   !< MESSAGE (Message from DLL to simulation code [ErrMsg])         
      ! Initialize the avrSWAP array (from HAWC2 to DISCON DLL):
      IF (callno .eq. 0) THEN
         callno = 1
		 arrayin = sngl(0.0)
		 arrayout = sngl(0.0)
		 avrSWAP( 3) = 0.02
		 deltat = avrSWAP( 3)
	  ELSE
	     avrSWAP( 1) = 1.0
		 avrSWAP( 3) = 0.02
		 deltat = avrSWAP( 3)
      ENDIF	 


      ! Update swap array with values from HAWC2 before calling DISCON dll
      avrSWAP(1)  = arrayin(1)            ! 1. HAWC2 status
      avrSWAP(2)  = arrayin(2)            ! 2. Time[s]
      avrSWAP(20) = arrayin(3)*GearRatio  ! 3. HSS speed (input is LSS) [rad/s]
      avrSWAP(4)  = arrayin(4)            ! 4. Blade 1 pitch angle [rad]
      avrSWAP(33) = arrayin(5)            ! 5. Blade 2 pitch angle [rad]
      avrSWAP(34) = arrayin(6)            ! 6. Blade 3 pitch angle [rad]
      avrSWAP(27) = arrayin(7)            ! 7. Hub wind speed [m/s]
      avrSWAP(61) = NumBld                ! Number of blades


	  ! Compare to string in first line of block below (count number of characters and add 1 for C_NULL_CHAR)!!!
	  avrSWAP(50) = sngl(24.0)  ! Number of characters in Input File. 
	  avrSWAP(35) = sngl(1.0e0)  ! Generator contactor status: 1=main (high speed) variable-speed generator
        
      
      ! Call the DISCON dll to update the swap array
      accINFILE  = TRANSFER( TRIM('./control/BladedDLL.par')// C_NULL_CHAR, accINFILE)
      CALL C_F_PROCPOINTER(TRANSFER(proc_address, C_NULL_FUNPTR), bladed_proc)
      CALL bladed_proc( avrSWAP, aviFAIL, accINFILE, avcOUTNAME, avcMSG )
	  
      ! Use collective pitch
      CollectivePitch = avrSWAP(45)
      
      ! Pass values back to HAWC. Accessible via HAWC output. First 1 is actions, blade pitch passed to servo.
      arrayout(1) = dble(avrSWAP(47))*GearRatio              ! Demanded generator torque (LSS side) [Nm]
	  arrayout(2) = dble(CollectivePitch)                    ! Blade 1 pitch [rad]
	  arrayout(3) = dble(CollectivePitch)                    ! Blade 2 pitch [rad]
	  arrayout(4) = dble(CollectivePitch)                    ! Blade 3 pitch [rad]
      arrayout(5) = dble(avrSWAP(20)*avrSWAP(47)*GenEff)     ! Electrical Power [kW]
	  arrayout(6) = dble(avrSWAP(20)*30/pi)                  ! Generator Speed [rpm]


      ! Output certain swap-array values to logfile if requested
      IF (EnableDllLog .eqv. .true.) THEN
         open(unit=99, file='_DllLog.txt', access = 'append', status='old') 
         write (99,*) avrSWAP(2), avrSWAP(20), avrSWAP(47),  & ! time, gen speed, rot trq
                      avrSWAP(45), avrSWAP(61)                 ! pitch angle, no. blade
         close(unit=99)
      ENDIF
      
   END SUBROUTINE 
      
   !**************************************************************************************************

END MODULE NREL_5MW_INTERFACE
